﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySaleGroupTestTask.Core.Classes;

namespace MySaleGroupTestTask.UnitTests.TreeBuilderTests
{
    [TestClass]
    public class UpdateNode
    {
        [TestMethod]
        public void UpdateSuccessfully()
        {
            TreeBuilder tree = new TreeBuilder();
            tree.AddRootNode("1", "root");

            var id = "2";
            tree.AddNode("1", id, "second");

            var newName = "third";
            tree.UpdateNode(id, newName);

            Assert.AreEqual(tree.GetNode(id).Name, newName);
        }
    }
}
