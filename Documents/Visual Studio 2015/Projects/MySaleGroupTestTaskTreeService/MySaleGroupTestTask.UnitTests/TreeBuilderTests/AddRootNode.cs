﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySaleGroupTestTask.Core.Classes;

namespace MySaleGroupTestTask.UnitTests.TreeBuilderTests
{
    [TestClass]
    public class AddRootNode
    {
        TreeBuilder TreeTest = new TreeBuilder();

        [TestMethod]
        public void ShouldBeOnlyOneRootNode()
        {           
            TreeTest.AddRootNode("1", "firstRoot");
            try
            {
                TreeTest.AddRootNode("2", "secondRoot");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex != null);             
            }
        }

        [TestMethod]
        public void RootNodeAddedSuccesfullyToEmptyTree()
        {    
           TreeTest.AddRootNode("1", "firstRoot");
           var rootNode = TreeTest.GetRootNode();
           Assert.IsNotNull(rootNode);          
        }        
    }
}
