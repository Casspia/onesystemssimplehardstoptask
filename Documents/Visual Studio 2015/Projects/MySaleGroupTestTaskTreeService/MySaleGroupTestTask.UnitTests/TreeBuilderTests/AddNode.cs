﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySaleGroupTestTask.Core.Classes;

namespace MySaleGroupTestTask.UnitTests.TreeBuilderTests
{
    [TestClass]
    public class AddNode
    {
        TreeBuilder TreeTest = new TreeBuilder();         

        [TestMethod]
        public void NodeWithSameIdShouldThrowException()
        {
            try
            {
                TreeTest.AddRootNode("2", "second");
                TreeTest.AddRootNode("2", "third");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex != null);
            }
        }
    }
}
