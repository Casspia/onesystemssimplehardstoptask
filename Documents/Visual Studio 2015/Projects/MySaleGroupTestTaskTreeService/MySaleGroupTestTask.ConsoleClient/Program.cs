﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySaleGroupTestTask.Core.Classes;

namespace MySaleGroupTestTask.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello");
            var tree = new TreeBuilder();
            tree.AddRootNode("1", "first");
            tree.AddNode("1", "2", "second");
            tree.Traverse(node => Console.WriteLine(node));
      
            Console.ReadKey();
        }
    }
}
