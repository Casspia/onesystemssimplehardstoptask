﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySaleGroupTestTaskTreeService.Classes;
using MySaleGroupTestTaskTreeService.Interfaces;
namespace MySaleGroupTestTaskTreeService
{
    class Program
    {
        static void Main(string[] args)
        {
            var tb = new TreeBuilder();
            tb.AddRootNode("1", "1");
            tb.AddNode("1", "2", "2");
            tb.AddNode("1", "3", "3");
            tb.AddNode("1", "4", "4");
            tb.AddNode("3", "5", "5");
            tb.AddNode("3", "6", "6");
            /*Console.WriteLine("Before");
            foreach (var node in tb._allNodes)
            {
                Console.WriteLine(node);
            }
            tb.RemoveNode("3");
            Console.WriteLine("After");
            foreach (var node in tb._allNodes)
            {
                Console.WriteLine(node);
            }*/
            Action<ITreeNode> writeNode = node => Console.WriteLine(node);

            Console.WriteLine("Before import");
            tb.Traverse(writeNode);

            Console.WriteLine("After import");
            tb.Import(new List<ITreeListNode>{ new TreeListNode("2", "1", "*2*"), new TreeListNode("7", "6", "7") });

            tb.Traverse(writeNode);

            Console.ReadKey();
        }
    }
}
