﻿using MySaleGroupTestTaskTreeService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTaskTreeService.Classes
{
    public class TreeListNode : ITreeListNode
    {
        string _id;
        string _parentId;
        string _name;

        public TreeListNode(string id, string parentId, string name)
        {
            _id = id;
            _parentId = parentId;
            _name = name;
        }

        public string Id { get { return _id; } }
        public string ParentId { get { return _parentId; } }
        public string Name { get { return _name; } }
    }
}
