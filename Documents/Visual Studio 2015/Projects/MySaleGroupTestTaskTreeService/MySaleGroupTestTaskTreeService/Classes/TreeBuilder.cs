﻿using MySaleGroupTestTaskTreeService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTaskTreeService.Classes
{
    public class TreeBuilder : ITreeBuilder
    {
        private ITreeNode _rootNode;
        // TODO: make it private
        public List<ITreeNode> _allNodes = new List<ITreeNode>();

        /// <summary>
        /// Adds root node to the tree. Root node is a node without parent, all other nodes should have parent. Only one root node can
        ///exists in a tree, if root node already exists exception should be thrown.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void AddRootNode(string id, string name)
        {
            if (_rootNode == null)
            {
                _rootNode = new TreeNode(id, null, name);
                _allNodes.Add(_rootNode);
            }
            else
            {
                throw new Exception("There is root node already!");
            }
        }
        /// <summary>
        /// Adds node to the tree, with particular parent specified. If this parent doesn't exist exception should be thrown. If node with the
        /// same Id exists in the tree exception should be thrown.
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <param name="name"></param>
        public void AddNode(string parentId, string id, string name)
        {
            if (_rootNode == null) throw new Exception("There is no root");

            ITreeNode parentNode = GetNode(parentId);

            if (parentNode == null) throw new Exception("Parent doesnt exist");

            var nodeWithSameId = GetNode(id);
            if (nodeWithSameId != null) throw new Exception("Node with this id exists");

            var newNode = new TreeNode(id, parentNode as TreeNode, name);
            parentNode.Children.Add(newNode);
            _allNodes.Add(newNode);
        }
        /// <summary>
        /// Updates node in the tree. If this node doesn't exists exception should be thrown.
        /// </summary>     
        public void UpdateNode(string id, string newName)
        {
            var nodeToUpdate = GetNode(id);
            if (nodeToUpdate == null) throw new Exception("Nothing to update");

            nodeToUpdate.Name = newName;
        }

        /// <summary>
        /// Removes node from the tree with all their children. If this node doesn't exists exception should be thrown.
        /// </summary>
        /// <param name="id"></param>
        public void RemoveNode(string id)
        {
            var nodeToRemove = GetNode(id);
            if(nodeToRemove == null) throw new Exception("Nothing to delete");

            nodeToRemove.Parent.Children.Remove(nodeToRemove);

            RemoveNodeRecursive(nodeToRemove);
        }

        private void RemoveNodeRecursive(ITreeNode node)
        {
            if (node != null)
            {
                foreach (var child in node.Children)
                {
                    RemoveNodeRecursive(child);
                }
                _allNodes.Remove(node);
            }
        }

        /// <summary>
        /// Returns root node of the tree. If this node doesn't exists should return null.
        /// </summary>
        /// <returns></returns>
        public ITreeNode GetRootNode()
        {
            return _rootNode;
        }
        /// <summary>
        /// Returns specified node from the tree. If this node doesn't exists should return null.
        /// </summary>       
        public ITreeNode GetNode(string id)
        {
            return _allNodes.Where(node => node.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// Traverses the tree and calls the action passed for each tree node.
        /// </summary>
        /// <param name="action"></param>
        public void Traverse(Action<ITreeNode> action)
        {
            if (_rootNode != null)
            {
                TraverseRecursive(_rootNode, action);
            }
        }

        private void TraverseRecursive(ITreeNode node, Action<ITreeNode> action)
        {
            if (node != null)
            {
                action(node);
                foreach (var child in node.Children)
                {
                    TraverseRecursive(child,action);
                }             
            }
        }

        /// <summary>
        /// Imports tree from the flat list of nodes. Overwrites all the previous nodes.
        /// </summary>
        /// <param name="nodes"></param>
        public void Import(IEnumerable<ITreeListNode> nodes)
        {
            foreach (var treeListNode in nodes)
            {
                var parentNode = GetNode(treeListNode.ParentId);
                if (parentNode != null)
                {
                    var existingNode = GetNode(treeListNode.Id);

                    if (existingNode != null)
                    {
                        UpdateNode(treeListNode.Id, treeListNode.Name);                  
                    }
                    else
                    {
                        AddNode(treeListNode.ParentId, treeListNode.Id, treeListNode.Name);
                    }
                }
            }
        }

        /// <summary>
        /// Exports tree to the flat list of nodes.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ITreeListNode> Export()
        {
            foreach (var node in _allNodes)
            {
                yield return new TreeListNode(node.Id,node.Parent == null ? null : node.Parent.Id,node.Name);
            }
        }
    }
}
