﻿using MySaleGroupTestTaskTreeService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTaskTreeService.Classes
{
    public class TreeNode : ITreeNode
    {
        string _id;
        ITreeNode _parent;
        string _name;
        List<ITreeNode> _children = new List<ITreeNode>();

        public TreeNode(string id, TreeNode parent, string name)
        {
            _id = id;
            _parent = parent;
            _name = name;
        }

        public string Id { get { return _id; } }
        public ITreeNode Parent { get { return _parent; } }
        public List<ITreeNode> Children { get { return _children; } }
        public string Name { get { return _name; } set { _name = value; } }

        public override string ToString()
        {
            return string.Format("node = id:{0}, parentId:{1}, name: {2}",_id,_parent == null? "null": _parent.Id,_name);
        }
    }
}
