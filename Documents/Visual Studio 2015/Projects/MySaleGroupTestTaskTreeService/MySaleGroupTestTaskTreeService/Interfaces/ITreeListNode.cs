﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTaskTreeService.Interfaces
{
    public interface ITreeListNode
    {
        string Id { get; }
        string ParentId { get; }
        string Name { get; }
    }
}
