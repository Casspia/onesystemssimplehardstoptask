﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using Newtonsoft.Json;
using MySaleGroupTestTask.Core.Classes;

namespace MySaleGroupTestTask.WebService.Controllers
{
    public class WebTreeBuilder : TreeBuilder
    {
        public string TreeBuilderId { get; set; }
        public int NodesCount {
            get {
                return this.Export().Count();
            }
        }
    }
    static class TreeBuilderSessionStorage
    {
        private static string treeBuilderCollectionId = "az532d6b43dr";
        private static List<WebTreeBuilder> treeBCollection = new List<WebTreeBuilder>();

        static TreeBuilderSessionStorage()
        {
            HttpContext.Current.Session.Add(treeBuilderCollectionId,treeBCollection);
        }

        public static WebTreeBuilder GetById(string Id)
        {
            var tbc = HttpContext.Current.Session[treeBuilderCollectionId] as List<WebTreeBuilder>;

            if (tbc.Count(wtb => wtb.TreeBuilderId == Id) == 0)
            {
                throw new Exception("No treebulder with id = " + Id);
            }
            else
            {
                return tbc.Where(wtb => wtb.TreeBuilderId == Id).FirstOrDefault();
            }
        }

        public static List<WebTreeBuilder> GetAllWTB()
        {
            var tbc = HttpContext.Current.Session[treeBuilderCollectionId] as List<WebTreeBuilder>;
            return tbc;
        }

        public static void AddById(string Id)
        {
            var tbc = HttpContext.Current.Session[treeBuilderCollectionId] as List<WebTreeBuilder>;
            var newTB = new WebTreeBuilder() { TreeBuilderId = Id };
            newTB.AddRootNode(Id, Id);
            tbc.Add(newTB);
        }
    }
    public class TreeBuilderController : ApiController
    {
        // GET: api/TreeBuilder
        /// <summary>
        /// get all TreeBuilders
        /// </summary>
        /// <returns></returns>
        public string Get()
        {
            return JsonConvert.SerializeObject(TreeBuilderSessionStorage.GetAllWTB());
        }

        // GET: api/TreeBuilder/5
        public string Get(string id)
        {
            var tb = TreeBuilderSessionStorage.GetById(id);
            // return JsonConvert.SerializeObject(new TreeNode(id, new TreeNode("1", null, "1"), id));
            return JsonConvert.SerializeObject(tb.Export());
        }

        // POST: api/TreeBuilder
        public void Post([FromBody]string value, string Id)
        {
            var nodeToUpdate = JsonConvert.DeserializeObject<TreeListNode>(value);
            var tb = TreeBuilderSessionStorage.GetById(Id);
            tb.UpdateNode(nodeToUpdate.Id, nodeToUpdate.Name);
        }

        // PUT: api/TreeBuilder/5
        public void Put(string id, [FromBody]string value)
        {
            // create new tree builder
            if (value == null)
            {
                TreeBuilderSessionStorage.AddById(id);
            }
            // insert new node
            else
            {
                var nodeToInsert = JsonConvert.DeserializeObject<TreeListNode>(value);
                var tb = TreeBuilderSessionStorage.GetById(id);
                tb.AddNode(nodeToInsert.ParentId, nodeToInsert.Id, nodeToInsert.Name);
            }
        }


        // DELETE: api/TreeBuilder/5
        public string Delete(string Id, [FromBody]string value)
        {
            if (value != null)
            {
                var nodeToDelete = JsonConvert.DeserializeObject<TreeListNode>(value);
                var tb = TreeBuilderSessionStorage.GetById(Id);
                tb.RemoveNode(nodeToDelete.Id);
                return String.Format("Node with Id {0} successfully deleted", nodeToDelete.Id);
            }
            else
            {
                throw new Exception("Provide tree id that you want to delete");
            }         
        }
    }
}
