﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTask.Core.Interfaces
{
    public interface ITreeBuilder
    {
        void AddRootNode(string id, string name);
        void AddNode(string parentId, string id, string name);
        void UpdateNode(string id, string newName);
        void RemoveNode(string id);
        ITreeNode GetRootNode();
        ITreeNode GetNode(string id);
        void Traverse(Action<ITreeNode> action);
        void Import(IEnumerable<ITreeListNode> nodes);
        IEnumerable<ITreeListNode> Export();
    }
}
