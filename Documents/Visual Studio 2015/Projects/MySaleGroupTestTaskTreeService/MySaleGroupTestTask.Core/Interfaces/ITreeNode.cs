﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySaleGroupTestTask.Core.Interfaces
{
    public interface ITreeNode
    {
        string Id { get; }
        ITreeNode Parent { get; }
        List<ITreeNode> Children { get; }
        string Name { get; set; }
    }
}
