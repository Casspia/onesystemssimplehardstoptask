﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySaleGroupTestTaskTreeService.Classes;

namespace UnitTestProject
{
    [TestClass]
    public class TreeBuilderTests
    {
        [TestMethod]
        public void Should_be_only_one_root()
        {
            var tb = new TreeBuilder();
            tb.AddRootNode("1", "root");

            try
            {
              tb.AddRootNode("2", "root2");
            }
            catch (Exception)
            { 
                Assert.AreEqual("exception", "exception");
            } 

        }
    }
}
